use sdm;

# Jumlah komplain setiap bulan
SELECT CONCAT(YEAR(Date_Received),'-',MONTH(Date_Received)) AS tahun_bulan, COUNT(*) AS jumlah_komplain FROM complaints
GROUP BY YEAR(Date_Received),MONTH(Date_Received);

# Komplain yang memiliki tags ‘Older American’
select * from complaints where Tags = 'Older American';

# Buat sebuah view yang menampilkan data nama perusahaan, jumlah company response to consumer
SELECT Company                                                                     AS Company,
       count(Company_Response_to_Consumer)                                         as Complaints,
       sum(IF(Company_Response_to_Consumer = 'Closed', 1, 0))                      AS Closed,
       sum(IF(Company_Response_to_Consumer = 'Closed with explanation', 1, 0))     as Closed_with_explanation,
       sum(IF(Company_Response_to_Consumer = 'Closed with monetary relief', 1, 0)) as Closed_with_monetary_relief,
       sum(IF(Company_Response_to_Consumer = 'Closed with non-monetary relief', 1, 0)) as Closed_with_non_monetary_relief

FROM complaints GROUP BY Company;